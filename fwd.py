from amaranth import *
from regfile import RegNames
from enum import Enum


class ForwardSources(Enum):
    REG = 0
    EX = 3
    MEM = 2
    WB = 1


class Forwarder(Elaboratable):
    def __init__(self):
        self.src = Array(
            Signal(2, name=f"reg{i}_src", decoder=ForwardSources) for i in range(32))
        self.do_set = Signal()
        self.which_rs1 = Signal(5, decoder=RegNames)
        self.which_rs2 = Signal(5, decoder=RegNames)
        self.which_rd = Signal(5, decoder=RegNames)
        self.build_rd = Signal()

    def elaborate(self, platform):
        m = Module()

        # Decrement if not already written to regfile
        for reg in range(32):
            with m.If(self.src[reg] != 0):
                m.d.sync += self.src[reg].eq(self.src[reg]-1)
            with m.Else():
                m.d.sync += self.src[reg].eq(self.src[reg])

        # In the next period, rd will be available at the end of EX
        with m.If((self.do_set == 1) & (self.build_rd)):
            m.d.sync += self.src[self.which_rd].eq(ForwardSources.EX)

        m.d.sync += self.src[0].eq(ForwardSources.REG)

        return m
