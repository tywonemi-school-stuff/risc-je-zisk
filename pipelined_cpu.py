from amaranth import *
from port import Port
from stages import *
from datamem import DataMemMode
from fwd import ForwardSources


class PipelinedProcessor(Elaboratable):
    def __init__(self):
        self.i_bus = Port(32)
        self.dr_bus = Port(32)
        self.dw_bus = Port(32)
        self.fetch = StageIF(self.i_bus)
        self.id = StageID(prev_stage=self.fetch)
        self.ex = StageEX(prev_stage=self.id)
        self.mem = StageMEM(self.dr_bus, self.dw_bus, prev_stage=self.ex)
        self.wb = StageWB(prev_stage=self.mem)

    def elaborate(self, platform):
        m = Module()
        fetch = m.submodules.fetch = self.fetch
        id = m.submodules.id = self.id
        ex = m.submodules.ex = self.ex
        mem = m.submodules.mem = self.mem
        wb = m.submodules.wb = self.wb
        connect_stages(m, fetch, id)
        connect_stages(m, id, ex)
        connect_stages(m, ex, mem)
        connect_stages(m, mem, wb)

        m.d.sync += [
            fetch.stall_from_top_next.eq(0),
            id.stall_from_top_next.eq(0),
            ex.stall_from_top_next.eq(0),
        ]
        with m.If(id.stall_because_read != 0):
            m.d.comb += [
                fetch.stall_from_top.eq(1),
                id.stall_from_top.eq(1),
            ]
        with m.Else():
            m.d.sync += fetch.pc.eq(fetch.pc+4)
        with m.If(ex.do_jump != 0):
            m.d.sync += [
                id.stall_from_top_next.eq(1),
                ex.stall_from_top_next.eq(1),
            ]
            m.d.sync += fetch.pc.eq(ex.brancher.new_pc)

        m.d.comb += [
            id.which_rd_from_wb.eq(wb.which_rd),
            id.rd_from_wb.eq(wb.rd),
            id.mux_rs1[ForwardSources.EX.value].eq(ex.out_alu_out),
            id.mux_rs1[ForwardSources.MEM.value].eq(mem.rd),
            id.mux_rs1[ForwardSources.WB.value].eq(wb.rd),
            id.mux_rs2[ForwardSources.EX.value].eq(ex.out_alu_out),
            id.mux_rs2[ForwardSources.MEM.value].eq(mem.rd),
            id.mux_rs2[ForwardSources.WB.value].eq(wb.rd),
            id.ex_is_mem_read.eq(
                (ex.out_mem_mode == DataMemMode.READ) & (~ex.true_stall)),
            id.ex_is_jump.eq(ex.do_jump & (~ex.true_stall)),
        ]

        return m
