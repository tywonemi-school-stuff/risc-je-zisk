from enum import *
from amaranth import *


class ALUOp(Enum):
    # funct3 values for ALU operations
    SUM = 0b000
    LESS_THAN = 0b010
    LESS_THAN_UNSIGNED = 0b011
    XOR = 0b100
    OR = 0b110
    AND = 0b111
    SHIFT_L = 0b001
    SHIFT_R = 0b101  # ARITHMETC decided by bit 30 of instr


class ALUSource1(Enum):
    ZERO = auto()
    PC = auto()
    RS1 = auto()


class ALUSource2(Enum):
    IMM = auto()
    RS2 = auto()


class Alu(Elaboratable):
    def __init__(self):
        self.pc = Signal(32)
        self.rs1 = Signal(signed(32))
        self.rs2 = Signal(signed(32))
        self.out = Signal(signed(32))
        self.imm = Signal(signed(32))
        self.which_rs2 = Signal(5)
        self.shamt_is_rs2 = Signal()
        self.alu_op = Signal(3, decoder=ALUOp)
        self.alu_src1 = Signal(2, decoder=ALUSource1)
        self.alu_src2 = Signal(2, decoder=ALUSource2)

        self.shift_arithmetic = Signal()
        self.sub = Signal()
        self.invalid = Signal()

    def elaborate(self, platform):
        m = Module()

        src1 = Signal(32)
        src2 = Signal(32)
        shamt = Signal(5)

        with m.Switch(self.alu_src1):
            with m.Case(ALUSource1.ZERO):
                pass
            with m.Case(ALUSource1.PC):
                m.d.comb += src1.eq(self.pc)
            with m.Case(ALUSource1.RS1):
                m.d.comb += src1.eq(self.rs1)
            with m.Default():
                m.d.comb += self.invalid.eq(True)

        with m.If(self.shamt_is_rs2):
            # R-type
            m.d.comb += shamt.eq(self.rs2)
        with m.Else():
            # I-type
            m.d.comb += shamt.eq(self.which_rs2)

        with m.Switch(self.alu_src2):
            with m.Case(ALUSource2.IMM):
                m.d.comb += src2.eq(self.imm)
            with m.Case(ALUSource2.RS2):
                m.d.comb += src2.eq(self.rs2)
            with m.Default():
                m.d.comb += self.invalid.eq(True)

        with m.Switch(self.alu_op):
            with m.Case(ALUOp.SUM):
                with m.If(self.sub):
                    m.d.comb += self.out.eq(src1 - src2)
                with m.Else():
                    m.d.comb += self.out.eq(src1 + src2)
            with m.Case(ALUOp.LESS_THAN):
                m.d.comb += self.out.eq(src1.as_signed() < src2.as_signed())
            with m.Case(ALUOp.LESS_THAN_UNSIGNED):
                m.d.comb += self.out.eq(src1.as_unsigned()
                                        < src2.as_unsigned())
            with m.Case(ALUOp.XOR):
                m.d.comb += self.out.eq(src1 ^ src2)
            with m.Case(ALUOp.OR):
                m.d.comb += self.out.eq(src1 | src2)
            with m.Case(ALUOp.AND):
                m.d.comb += self.out.eq(src1 & src2)
            with m.Case(ALUOp.SHIFT_L):
                m.d.comb += self.out.eq(src1 << shamt)
            with m.Case(ALUOp.SHIFT_R):
                with m.If(self.shift_arithmetic):
                    m.d.comb += self.out.eq(src1.as_signed() >> shamt)
                with m.Else():
                    m.d.comb += self.out.eq(src1 >> shamt)
            with m.Default():
                m.d.comb += self.invalid.eq(True)

        return m
