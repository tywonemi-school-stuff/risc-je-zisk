const std = @import("std");

export fn microzig_start() void {
    main() catch unreachable;
}

pub fn main() !void { 
    asm volatile ("la sp, microzig_data_load_start");
    asm volatile ("add s0, sp, zero");
    // Test ADD
    asm volatile ("li a0, 20");
    asm volatile ("li a1, 10");
    asm volatile ("add a2, a0, a1");
    asm volatile ("li a5, 30");
    asm volatile ("bne a5, a2, die");
    // Test SUB
    asm volatile ("sub a2, a1, a0");
    asm volatile ("li a5, -10");
    asm volatile ("bne a5, a2, die");
    // Test SLLI
    asm volatile ("slli a2, a1, 2");
    asm volatile ("li a5, 40");
    asm volatile ("bne a5, a2, die");
    // Test SLL
    asm volatile ("sll a2, a0, a1");
    asm volatile ("li a5, 20480");
    asm volatile ("bne a5, a2, die");
    // Test SRLI
    asm volatile ("li a0, -3");
    asm volatile ("li a1, 1");
    asm volatile ("srli a2, a0, 1");
    asm volatile ("li a5, 2147483646");
    asm volatile ("bne a5, a2, die");
    // Test SRAI
    asm volatile ("srai a2, a0, 1");
    asm volatile ("li a5, -2");
    asm volatile ("bne a5, a2, die");
    // Test SRL
    asm volatile ("srl a2, a0, a1");
    asm volatile ("li a5, 2147483646");
    asm volatile ("bne a5, a2, die");
    // Test LW, SW
    asm volatile ("li a0, 5");
    asm volatile ("sw a0, 12(sp)");
    asm volatile ("li a0, 99");
    asm volatile ("lw a0, 12(sp)");
    asm volatile ("li a5, 5");
    asm volatile ("bne a5, a0, die");
    // Test LB, SB
    asm volatile ("li a0, 5");
    asm volatile ("sb a0, 12(sp)");
    asm volatile ("li a0, 99");
    asm volatile ("lb a0, 12(sp)");
    asm volatile ("li a5, 5");
    asm volatile ("bne a5, a0, die");
    // Test calling a real function
    // var fib9: u32 = fibonacci(9);
    // asm volatile ("li a5, 34");
    // asm volatile ("bne a5, a0, die");
    _ = @call(.{.modifier = .never_inline}, gcd, .{14, 21});
    asm volatile ("li a5, 7");
    asm volatile ("bne a5, a0, die");
    asm volatile ("li a1, 0x0D15EA5E");
    die();
}

export fn die() noreturn {
    asm volatile ("nop");
    asm volatile ("nop");
    asm volatile ("nop");
    asm volatile ("nop");
    asm volatile ("nop");
    asm volatile ("ebreak");
    while (true) {}
}

// returns value of Fibonacci series at index idx
pub fn fibonacci(idx: u32) u32 {
    var t1: u32 = 0;
    var t2: u32 = 1;
    var nextTerm: u32 = t1 + t2;
    var i: u32 = 3;
    while (i <= idx) {
        t1 = t2;
        t2 = nextTerm;
        nextTerm = t1 + t2;
        i += 1;
    }

    return nextTerm;
}

pub fn gcd(n1: u32, n2: u32) u32 {
    var tn1: u32 = n1;
    var tn2: u32 = n2;

    while (tn1 != tn2) {
        if (tn1 > tn2) {
            tn1 -= tn2;
        } else {
            tn2 -= tn1;
        }
    }
    return tn1;
}
