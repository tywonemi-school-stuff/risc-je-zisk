const std = @import("std");
const microzig = @import("microzig/src/main.zig");

pub fn build(b: *std.build.Builder) !void {
    const backing = .{
        // if you don't have one of the boards, but do have one of the
        // "supported" chips:
        .chip = microzig.chips.risc_je_zisk,
    };
    // No extensions beyond RV32I
    // const model = std.Target.riscv.cpu.generic_rv32;
    // const target: std.zig.CrossTarget = .{
    //     .cpu_arch = .riscv32,
    //     .cpu_model = .{ .explicit = &model },
    //     .os_tag = .freestanding,
    //     .abi = .none,
    // };
    // Standard release options allow the person running `zig build` to select
    // between Debug, ReleaseSafe, ReleaseFast, and ReleaseSmall.
    // const mode = b.standardReleaseOptions();

    // const exe = b.addExecutable("fw", "src/main.zig");
    const exe = try microzig.addEmbeddedExecutable(
        b,
        "fw",
        "src/main.zig",
        backing,
        .{
          // optional slice of packages that can be imported into your app:
          // .packages = &my_packages,
        },
    );
    // exe.setTarget(target);
    // exe.setLinkerScriptPath(std.build.FileSource.relative("linkerscript.ld"));
    exe.setBuildMode(.ReleaseSmall);
    exe.install();
}
