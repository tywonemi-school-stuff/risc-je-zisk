from amaranth import *
from amaranth.hdl.rec import *


class InstrLayout(Layout):
    def __init__(self):
        super().__init__([
            ("addr", unsigned(16)),
            ("op", unsigned(7)),
            ("rd", unsigned(5)),
            ("funct3", unsigned(3)),
            ("rs1", unsigned(5)),
            ("rs2", unsigned(5)),
            ("funct7", unsigned(7)),
        ])


class Instruction(Record):
    def __init__(self):
        super().__init__(InstrLayout())
