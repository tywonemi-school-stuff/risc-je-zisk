# RISC je zisk

This is a RV32I processor, written in Amaranth HDL (formerly known as nMigen). ISA tests from https://github.com/riscv-software-src/riscv-tests are used to verify functionality.

Amaranth is a modern hardware description language with well-defined type conversions, composable designs, Python-enabled metaprogramming, formal verification and simulation, and a neat library of building blocks. Amaranth is distributed as a Python library, install it with `pip install amaranth`.

# Architecture

Single-cycle and 5-stage implementations are both built and tested by the `build.sh` script. Forwarding from any stage with the correct result is implemented. Branches are predicted not taken. Stalls are asserted synchronously (in the next clock cycle) in the correct stages when a branch is found to be taken. Stalls are also asserted combinatorially whenever an unfinished load is to be forwarded from the execution stage.

Memory is implemented using a 32-bit word wide instance of the Memory object from the Amaranth library. In order to implement byte and half-word accesses for data access, read-modify-write logic is used to mask the fact that our ports don't have control over the width, and are indexed with word addresses, meaning the bottom two bits of the real addresses are used in the shifting/masking of the words within my logic.

The flexibility of Python has proven to be helpful, most notably:

+ an ELF flattener within my design initializes the memory
+ immediate encoding and opcodes are neatly presented and logic is generated with standard Python control flow operations
+ pipeline stages are initialized with the previous stage, automatically inheriting all in-band outputs, by inheriting from a generic StageWrapper class

![cpu diagram](diagram.jpg?raw=true "CPU Diagram")

# My firmware

Make sure to `git submodule --init --recursive`! Instead of writing a linker script, I have added support to `microzig` which generates one through zig comptime.

You first need to build the firmware, so that it can be loaded into the simulated memory. The example minimal firmware is written in zig and targets RV32I with no specific ABI. Install zig and run `build.sh`. This also builds the standard ISA tests. If you want, you can install zig with `pip install zig`, but the canonical downloads are on the language's website or git.

# ISA tests

`./build.sh` also instantiates and simulates the design. I recommend using cutter/iaito to read an annotated assembly listing of the program. No GCC needed! Originally it was required, but since LLD now has functional relocs on this target, Zig does the job.

# Learn Amaranth

This is not the best example of Amaranth, the wiring could be tidier. If you want to learn Amaranth, start here:

+ [RobertBaruch's tutorial](https://github.com/robertbaruch/amaranth-tutorial)
+ [Amaranth docs](https://amaranth-lang.org/amaranth/latest/intro.html)
+ [larger example project: apertus open source cinema camera experimental nMigen gateware rewrite](https://github.com/apertus-open-source-cinema/naps)

# Requirements

+ amaranth 0.3
+ zig 0.9 or 0.10
+ riscv64-unknown-elf-binutils (this is an Arch community package that includes riscv32)
+ make
