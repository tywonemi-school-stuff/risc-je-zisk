#!/usr/bin/env bash
set -e

pushd fw
zig build install --prefix .
popd

mkdir -p test/isa/bin
pushd test/isa
RISCV_PREFIX=riscv32-none-elf- RISCV_OBJDUMP="$RISCV_PREFIX""objdump -Dz" XLEN=32 make rv32ui
popd

mkdir -p wave/single
mkdir -p wave/pipelined
python main.py
python main.py --pipelined
