from amaranth import *
from amaranth.asserts import Assert, Assume, Cover
from port import Port
from imm_compose import ImmDecoder
from signals import *
from controller import Controller
from regfile import RegFile
from branch import Brancher
from alu import Alu
from datamem import DataMem
from writeback import DestRegMux

class Processor(Elaboratable):
    def __init__(self):
        self.i_bus = Port(32)
        self.dr_bus = Port(32)
        self.dw_bus = Port(32)
        self.regfile = RegFile()
        self.pc = Signal(32)
        self.imm_decoder = ImmDecoder()
        self.controller = Controller()
        self.brancher = Brancher()
        self.alu = Alu()
        self.datamem = DataMem(self.dr_bus, self.dw_bus)
        self.writeback = DestRegMux()

    def elaborate(self, platform):
        m = Module()
        m.d.comb += [
            self.i_bus.addr.eq(self.pc[2:]),
        ]
        m.submodules.regfile = self.regfile
        m.submodules.imm_decoder = self.imm_decoder
        m.submodules.controller = self.controller
        m.submodules.brancher = self.brancher
        m.submodules.alu = self.alu
        m.submodules.datamem = self.datamem
        m.submodules.writeback = self.writeback
        funct3 = self.i_bus.data[12:15]
        m.d.comb += [
            self.imm_decoder.instruction.eq(self.i_bus.data),
            self.controller.instruction.eq(self.i_bus.data),
            self.regfile.which_rs1.eq(self.i_bus.data[15:20]),
            self.regfile.which_rs2.eq(self.i_bus.data[20:25]),
            self.brancher.pc.eq(self.pc),
            self.brancher.rs1.eq(self.regfile.rs1),
            self.brancher.rs2.eq(self.regfile.rs2),
            self.brancher.branch_op.eq(funct3),
            self.brancher.jump_en.eq(self.controller.jump_en),
            self.brancher.branch_en.eq(self.controller.branch_en),
            self.alu.alu_op.eq(self.controller.alu_op),
            self.alu.rs1.eq(self.regfile.rs1),
            self.alu.rs2.eq(self.regfile.rs2),
            self.alu.pc.eq(self.pc),
            self.alu.imm.eq(self.imm_decoder.imm),
            self.alu.which_rs2.eq(self.regfile.which_rs2),
            self.alu.shamt_is_rs2.eq(self.controller.shamt_is_rs2),
            self.alu.sub.eq(self.controller.sub),
            self.alu.shift_arithmetic.eq(self.controller.shift_arithmetic),
            self.alu.alu_src1.eq(self.controller.alu_src1),
            self.alu.alu_src2.eq(self.controller.alu_src2),
            self.brancher.pc_from_alu.eq(self.alu.out),
            self.datamem.width.eq(funct3),
            self.datamem.mode.eq(self.controller.mem_mode),
            self.datamem.write_data.eq(self.regfile.rs2),
            self.datamem.addr.eq(self.alu.out),
            self.writeback.wrd_sel.eq(self.controller.wrd_sel),
            self.writeback.mem_read.eq(self.datamem.read_data),
            self.writeback.alu_out.eq(self.alu.out),
            self.writeback.incr_pc.eq(self.brancher.incr_pc),
            self.regfile.rd.eq(self.writeback.rd),
        ]
        with m.If(self.controller.write_rd_en):
            m.d.comb += self.regfile.which_rd.eq(self.i_bus.data[7:12])

        m.d.sync += self.pc.eq(self.brancher.new_pc)

        return m
