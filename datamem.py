from amaranth import *
from enum import *
from port import Port


class DataMemMode(Enum):
    NONE = auto()
    READ = auto()
    WRITE = auto()


class DataMemWidth(Enum):
    BYTE = 0b000
    HALF = 0b001
    WORD = 0b010
    BYTE_UNSIGNED = 0b100
    HALF_UNSIGNED = 0b101


class DataMem(Elaboratable):
    def __init__(self, r_bus, w_bus):
        self.mode = Signal(2, decoder=DataMemMode)
        self.width = Signal(3, decoder=DataMemWidth)
        self.addr = Signal(32)
        self.read_data = Signal(32)
        self.write_data = Signal(32)
        self.dr_bus = r_bus
        self.dw_bus = w_bus

    def elaborate(self, platform):
        m = Module()

        read_data_shifted = Signal(32)
        # Bit offset of accessed
        bit_offset = 8*(self.addr[0:2])
        # Access word
        m.d.comb += self.dr_bus.addr.eq(self.addr[2:])
        m.d.comb += self.dw_bus.addr.eq(self.addr[2:])
        # Shift right so lowest byte or half word are the one we read
        m.d.comb += read_data_shifted.eq(self.dr_bus.data >> bit_offset)

        # To emulate byte and half word writes, we need to do read-modify-write.
        # The correct solution might be to extend amaranth to have sub-width mem access.
        with m.Switch(self.width):
            with m.Case(DataMemWidth.BYTE):
                m.d.comb += self.read_data.eq(
                    read_data_shifted[0:8].as_signed())
            with m.Case(DataMemWidth.BYTE_UNSIGNED):
                m.d.comb += self.read_data.eq(
                    read_data_shifted[0:8].as_unsigned())
            with m.Case(DataMemWidth.HALF):
                m.d.comb += self.read_data.eq(
                    read_data_shifted[0:16].as_signed())
            with m.Case(DataMemWidth.HALF_UNSIGNED):
                m.d.comb += self.read_data.eq(
                    read_data_shifted[0:16].as_unsigned())
            with m.Case(DataMemWidth.WORD):
                m.d.comb += self.read_data.eq(read_data_shifted)

        # We set this write data to read data. It's not written unless dw_en is set, anyway
        m.d.comb += self.dw_bus.data.eq(self.dr_bus.data)
        # And now we modify a part or all of it
        with m.If(self.mode == DataMemMode.WRITE):
            m.d.comb += self.dw_bus.en.eq(True)
            with m.Switch(self.width):
                with m.Case(DataMemWidth.BYTE):
                    m.d.comb += self.dw_bus.data.bit_select(
                        bit_offset, 8).eq(self.write_data[0:8])
                with m.Case(DataMemWidth.HALF):
                    m.d.comb += self.dw_bus.data.bit_select(
                        bit_offset, 16).eq(self.write_data[0:16])
                with m.Case(DataMemWidth.WORD):
                    m.d.comb += self.dw_bus.data.eq(self.write_data)

        return m
