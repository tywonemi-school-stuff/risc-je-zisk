from amaranth import *

from opcodes import Opcodes
from signals import *
from writeback import WritebackSource
from datamem import DataMemMode, DataMemWidth
from branch import BranchOp
from alu import *


class Controller(Elaboratable):
    def __init__(self):
        self.instruction = Signal(32)
        self.mem_mode = Signal(2, decoder=DataMemMode)
        self.mem_width = Signal(3, decoder=DataMemWidth)
        self.jump_en = Signal()
        self.branch_en = Signal()
        self.branch_op = Signal(3, decoder=BranchOp)
        self.write_rd_en = Signal()
        self.shamt_is_rs2 = Signal()
        self.shift_arithmetic = Signal()
        self.sub = Signal()
        self.wrd_sel = Signal(2, decoder=WritebackSource)
        self.alu_src1 = Signal(2, decoder=ALUSource1)
        self.alu_src2 = Signal(2, decoder=ALUSource2)
        self.alu_op = Signal(3, decoder=ALUOp)
        self.op = Signal(7, decoder=Opcodes)
        self.ebreak = Signal()
        self.need_rs1 = Signal(reset=1)
        self.need_rs2 = Signal(reset=1)
        self.build_rd = Signal(reset=1)

    def elaborate(self, platform):
        m = Module()
        maybe_shift_arithmetic_or_sub = self.instruction[30]
        op = self.op
        funct3 = self.instruction[12:15]
        # Defaults
        m.d.comb += [
            op.eq(self.instruction[0:7]),
            self.alu_src1.eq(ALUSource1.RS1),
            self.alu_src2.eq(ALUSource2.IMM),
            self.alu_op.eq(ALUOp.SUM),
            self.mem_mode.eq(DataMemMode.NONE),
        ]

        m.d.comb += self.mem_width.eq(funct3)
        with m.Switch(op):
            with m.Case(Opcodes.LOAD):
                m.d.comb += self.mem_mode.eq(DataMemMode.READ)
                m.d.comb += self.write_rd_en.eq(True)
                m.d.comb += self.wrd_sel.eq(WritebackSource.MEM)
                m.d.comb += self.need_rs2.eq(False)
            with m.Case(Opcodes.STORE):
                m.d.comb += self.mem_mode.eq(DataMemMode.WRITE)
                m.d.comb += self.build_rd.eq(False)
            with m.Case(Opcodes.BRANCH):
                m.d.comb += self.branch_en.eq(True)
                m.d.comb += self.branch_op.eq(funct3)
                m.d.comb += self.alu_src1.eq(ALUSource1.PC)
                m.d.comb += self.build_rd.eq(False)
            with m.Case(Opcodes.JALR):
                m.d.comb += self.jump_en.eq(True)
                m.d.comb += self.write_rd_en.eq(True)
                m.d.comb += self.wrd_sel.eq(WritebackSource.INCR_PC)
                m.d.comb += self.need_rs2.eq(False)
            with m.Case(Opcodes.JAL):
                m.d.comb += self.jump_en.eq(True)
                m.d.comb += self.write_rd_en.eq(True)
                m.d.comb += self.wrd_sel.eq(WritebackSource.INCR_PC)
                m.d.comb += self.alu_src1.eq(ALUSource1.PC)
                m.d.comb += self.need_rs1.eq(False)
                m.d.comb += self.need_rs2.eq(False)
            with m.Case(Opcodes.OP_IMM):
                m.d.comb += self.write_rd_en.eq(True)
                m.d.comb += self.wrd_sel.eq(WritebackSource.ALU)
                m.d.comb += self.alu_op.eq(funct3)
                m.d.comb += self.shift_arithmetic.eq(
                    maybe_shift_arithmetic_or_sub)
                m.d.comb += self.need_rs2.eq(False)
            with m.Case(Opcodes.OP):
                m.d.comb += self.shamt_is_rs2.eq(True)
                m.d.comb += self.write_rd_en.eq(True)
                m.d.comb += self.wrd_sel.eq(WritebackSource.ALU)
                m.d.comb += self.alu_src2.eq(ALUSource2.RS2)
                m.d.comb += self.alu_op.eq(funct3)
                m.d.comb += self.sub.eq(maybe_shift_arithmetic_or_sub)
                m.d.comb += self.shift_arithmetic.eq(
                    maybe_shift_arithmetic_or_sub)
            with m.Case(Opcodes.AUIPC):
                m.d.comb += self.write_rd_en.eq(True)
                m.d.comb += self.wrd_sel.eq(WritebackSource.ALU)
                m.d.comb += self.alu_src1.eq(ALUSource1.PC)
                m.d.comb += self.need_rs1.eq(False)
                m.d.comb += self.need_rs2.eq(False)
            with m.Case(Opcodes.LUI):
                m.d.comb += self.write_rd_en.eq(True)
                m.d.comb += self.wrd_sel.eq(WritebackSource.ALU)
                m.d.comb += self.alu_src1.eq(ALUSource1.ZERO)
                m.d.comb += self.need_rs1.eq(False)
                m.d.comb += self.need_rs2.eq(False)
            with m.Case(Opcodes.SYSTEM):
                m.d.comb += self.ebreak.eq(True)
                m.d.comb += self.need_rs2.eq(False)

        return m
