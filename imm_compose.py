from amaranth import *
import opcodes
from signals import *


class ImmDecoder(Elaboratable):
    def __init__(self):
        self.instruction = Signal(32)
        self.imm = Signal(32)
        self.invalid = Signal()
        self.op = Signal(7, decoder=opcodes.Opcodes)

    def elaborate(self, platform):
        m = Module()

        # When not assigned to from instruction bits, immediate bits are 0
        m.d.comb += self.imm.eq(0)

        m.d.comb += self.op.eq(self.instruction[0:7])
        op = self.op
        # If no opcode is matched, we raise the invalid flag
        m.d.comb += self.invalid.eq(True)
        for op_code in opcodes.Opcodes:
            with m.If(op == op_code.value):
                # An opcode matches, the instruction is valid
                m.d.comb += self.invalid.eq(False)
                op_map = opcodes.op_maps(op_code)
                for bit in range(32):
                    # If not left at 0, route instruction bits to form immediate
                    # This also implements immediate sign extension
                    if op_map[bit] != None:
                        m.d.comb += self.imm[bit].eq(
                            self.instruction[op_map[bit]])
        return m
