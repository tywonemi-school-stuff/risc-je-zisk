from amaranth import *
from elftools.elf.elffile import ELFFile
import math


def bytes_to_words(b, is_little_endian):
    length = int(math.ceil(len(b)/4))
    words = [0] * length
    if is_little_endian:
        for i in range(len(b)):
            words[i//4] += b[i] * 2**(8*(i % 4))
    else:
        for i in range(len(b)):
            words[i//4] += b[i] * 2**(8*(-i % 4))
    return words


class ELFRAM(Elaboratable):
    def __init__(self, file):
        self.memory = self.program(file)
        self.address = Signal(32)
        self.data = Signal(32)

    def elaborate(self, platform):
        m = Module()
        return m

    def program(self, file):
        elffile = ELFFile(file)
        # Figure out length of memory
        self.size = 0
        allocated_sections = [".text", ".reset", ".data", ".rodata", ".bss", ".stack"]
        copied_sections = [".text", ".reset", ".data", ".rodata"]
        for s in elffile.iter_sections():
            if s.name in allocated_sections:
                self.size = max(s.header.sh_addr + s.data_size, self.size)

        # print(f"Rebuilding memory: {self.size} Bytes")
        initial = [0] * int(math.ceil(float(self.size) / 4))
        for s in elffile.iter_sections():
            if s.name in copied_sections:
                start = s.header.sh_addr//4
                length = int(math.ceil(s.data_size/4))
                data = s.data()
                words = bytes_to_words(data, elffile.little_endian)
                initial[start:start + length] = words
                assert(length == len(words))
        return Memory(width=32, depth=self.size, init=initial, name="RAM")
