{
  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };

  outputs = { self, nixpkgs, flake-utils }:
    flake-utils.lib.eachDefaultSystem (system:
      let pkgs = import nixpkgs { inherit system; }; in {
        devShell = pkgs.mkShell {
          buildInputs = with pkgs; [
            zig_0_9
            pkgsCross.riscv32-embedded.stdenv.cc
            (python3.withPackages (ps: [ps.amaranth ps.pyelftools]))
          ];
        };
      }
    );
}
