from amaranth import *
from enum import *


class WritebackSource(Enum):
    ALU = auto()
    MEM = auto()
    INCR_PC = auto()


class DestRegMux(Elaboratable):
    def __init__(self):
        self.alu_out = Signal(32)
        self.mem_read = Signal(32)
        self.incr_pc = Signal(32)
        self.wrd_sel = Signal(2, decoder=WritebackSource)
        self.rd = Signal(32)
        self.invalid = Signal()

    def elaborate(self, platform):
        m = Module()

        with m.Switch(self.wrd_sel):
            with m.Case(WritebackSource.ALU):
                m.d.comb += self.rd.eq(self.alu_out)
            with m.Case(WritebackSource.MEM):
                m.d.comb += self.rd.eq(self.mem_read)
            with m.Case(WritebackSource.INCR_PC):
                m.d.comb += self.rd.eq(self.incr_pc)
            with m.Default():
                m.d.comb += self.invalid.eq(True)

        return m
