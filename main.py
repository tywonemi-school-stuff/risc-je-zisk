from os import listdir
import argparse
from regfile import RegNames
from pathlib import Path
from amaranth import *
from amaranth.sim import Simulator
from ram import *
from cpu import Processor
from pipelined_cpu import PipelinedProcessor

# SNAKE CRIMES: mitigate Amaranth issue #359
import sys
sys.setrecursionlimit(10000)


class Top(Elaboratable):
    def __init__(self, file, pipelined=True):
        if pipelined:
            self.cpu = m.submodules.cpu = PipelinedProcessor()
        else:
            self.cpu = m.submodules.cpu = Processor()
        self.mem = m.submodules.mem = ELFRAM(file)

    def elaborate(self, platform):
        m = Module()
        cpu = self.cpu
        mem = self.mem
        i_bus = m.submodules.i_bus = mem.memory.read_port(domain="comb")
        dr_bus = m.submodules.dr_bus = mem.memory.read_port(domain="comb")
        dw_bus = m.submodules.dw_bus = mem.memory.write_port()

        m.d.comb += [
            cpu.i_bus.data.eq(i_bus.data),
            cpu.dr_bus.data.eq(dr_bus.data),
            dw_bus.data.eq(cpu.dw_bus.data),
            i_bus.addr.eq(cpu.i_bus.addr),
            dr_bus.addr.eq(cpu.dr_bus.addr),
            dw_bus.addr.eq(cpu.dw_bus.addr),
            dw_bus.en.eq(cpu.dw_bus.en),
        ]
        return m


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description='RISC je zisk: a RV32I processor')
    parser.add_argument('--pipelined',  action='store_true', default=False,
                        help='use pipelined implementation (default: single-cycle)')
    args = parser.parse_args()
    if args.pipelined:
        print("Building pipelined design")
    else:
        print("Building single-cycle design")


    test_path = Path('test/isa/bin')
    tests = ['fw/bin/fw'] + [test_path/test for test in listdir(test_path)]
    failed_tests = []
    passed_tests = []
    borked_tests = []
    for file in tests:
        with open(file, 'rb') as f:
            m = Module()
            top = Top(f, pipelined=args.pipelined)
            m.submodules += top
            filestem = Path(Path(file).stem)
            sim = Simulator(m)
            if args.pipelined:
                wavefilestem = 'wave/pipelined'/filestem
                failure_reg_signal = top.cpu.id.regfile.regs[RegNames.A1.value]
                ebreak_signal = top.cpu.id.controller.ebreak
                traces = [
                    top.cpu.fetch.pc,
                    top.cpu.id.imm_decoder.imm,
                    top.cpu.id.imm_decoder.op,
                    top.cpu.id.regfile.which_rs1,
                    top.cpu.id.regfile.which_rs2,
                    top.cpu.id.regfile.which_rd,
                    top.cpu.ex.alu.rs1,
                    top.cpu.ex.alu.rs2,
                ]
            else:
                wavefilestem = 'wave/single'/filestem
                failure_reg_signal = top.cpu.regfile.regs[RegNames.A1.value]
                ebreak_signal = top.cpu.controller.ebreak
                traces = [
                    top.cpu.pc,
                    top.cpu.imm_decoder.imm,
                    top.cpu.imm_decoder.op,
                    top.cpu.regfile.which_rs1,
                    top.cpu.regfile.which_rs2,
                    top.cpu.regfile.which_rd,
                    top.cpu.alu.rs1,
                    top.cpu.alu.rs2,
                ]
            def process():
                for _ in range(1000):
                    op = yield ebreak_signal
                    if op == 1:
                        break
                    yield
                failure_register = yield failure_reg_signal
                if failure_register == 0x0D15EA5E:
                    passed_tests.append(filestem.name)
                elif failure_register == 0xDEADBEEF:
                    failed_tests.append(filestem.name)
                else:
                    borked_tests.append(filestem.name)
            sim.add_sync_process(process)
            sim.add_clock(1e-6)
            # dear non-POSIX users: my condolences
            print(f"\x1b[1K\rRunning test {filestem}", end='')
        with sim.write_vcd(f"{wavefilestem}.vcd", f"{wavefilestem}.gtkw", traces=traces):
            sim.run()
    print(f"\x1b[1K\r", end='')
    print(f"Passed: {passed_tests}")
    print(f"Failed: {failed_tests}")
    print(f"Borked: {borked_tests}")
    print(
        f"PROGRESS: {int(round(100*(len(passed_tests)/(len(passed_tests)+len(failed_tests)+len(borked_tests)))))}%")
