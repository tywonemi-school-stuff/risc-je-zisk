from enum import Enum
from amaranth import *
from amaranth.asserts import Assert, Assume, Cover


class RegNames(Enum):
    ZERO = 0
    RA = 1
    SP = 2
    GP = 3
    TP = 4
    T0 = 5
    T1 = 6
    T2 = 7
    S0 = 8
    S1 = 9
    A0 = 10
    A1 = 11
    A2 = 12
    A3 = 13
    A4 = 14
    A5 = 15
    A6 = 16
    A7 = 17
    S2 = 18
    S3 = 19
    S4 = 20
    S5 = 21
    S6 = 22
    S7 = 23
    S8 = 24
    S9 = 25
    S10 = 26
    S11 = 27
    T3 = 28
    T4 = 29
    T5 = 30
    T6 = 31


class RegFile(Elaboratable):
    def __init__(self):
        self.which_rs1 = Signal(5, decoder=RegNames)
        self.which_rs2 = Signal(5, decoder=RegNames)
        self.which_rd = Signal(5, decoder=RegNames)
        self.rs1 = Signal(32)
        self.rs2 = Signal(32)
        self.rd = Signal(32)
        self.regs = Array(Signal(32, name=f"reg{i}") for i in range(32))

    def elaborate(self, platform):
        m = Module()
        regs = self.regs
        m.d.comb += [
            self.rs1.eq(regs[self.which_rs1]),
            self.rs2.eq(regs[self.which_rs2])
        ]

        # We keep x0 at reset by never writing to it
        with m.If(self.which_rd != 0):
            m.d.sync += regs[self.which_rd].eq(self.rd)

        return m
