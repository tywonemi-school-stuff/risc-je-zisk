from amaranth import *


class Port():
    def __init__(self, width):
        self.addr = Signal(32)
        self.data = Signal(32)
        self.en = Signal()
