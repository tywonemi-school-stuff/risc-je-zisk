from enum import *


class Opcodes(Enum):
    LOAD   = 0b0000011
    STORE  = 0b0100011
    BRANCH = 0b1100011
    JALR   = 0b1100111
    JAL    = 0b1101111
    OP_IMM = 0b0010011
    OP     = 0b0110011
    AUIPC  = 0b0010111
    LUI    = 0b0110111
    SYSTEM = 0b1110011


class ImmEncodings(Enum):
    R = auto()
    I = auto()
    S = auto()
    B = auto()
    U = auto()
    J = auto()


XX = None
# integer I at list entry N means "keep immediate bit N at instruction bit I"
# XX at list entry N means "keep immediate bit N at constant 0"
#    31  30  29  28  27  26  25  24  23  22  21  20  19  18  17  16
#    15  14  13  12  11  10   9   8   7   6   5   4   3   2   1   0
encoding_map = {
    ImmEncodings.R:
    [XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX,
     XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX],
    ImmEncodings.I:
    [31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31,
     31, 31, 31, 31, 31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20],
    ImmEncodings.S:
    [31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31,
     31, 31, 31, 31, 31, 30, 29, 28, 27, 26, 25, 11, 10,  9,  8,  7],
    ImmEncodings.B:
    [31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31,
     31, 31, 31, 31,  7, 30, 29, 28, 27, 26, 25, 11, 10,  9,  8, XX],
    ImmEncodings.U:
    [31, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16,
     15, 14, 13, 12, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX, XX],
    ImmEncodings.J:
    [31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 31, 19, 18, 17, 16,
     15, 14, 13, 12, 20, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, XX],
}

op_types = {
    Opcodes.OP_IMM: ImmEncodings.I,
    Opcodes.LUI: ImmEncodings.U,
    Opcodes.AUIPC: ImmEncodings.U,
    Opcodes.OP: ImmEncodings.R,
    Opcodes.JAL: ImmEncodings.J,
    Opcodes.JALR: ImmEncodings.I,
    Opcodes.BRANCH: ImmEncodings.B,
    Opcodes.LOAD: ImmEncodings.I,
    Opcodes.STORE: ImmEncodings.S,
    Opcodes.SYSTEM: ImmEncodings.I,
}


def op_maps(op):
    map = encoding_map[op_types[op]].copy()
    map.reverse()
    return map
