from amaranth import *
from fwd import Forwarder, ForwardSources
from regfile import *
from alu import Alu
from branch import Brancher
from controller import Controller
from imm_compose import ImmDecoder
from datamem import *
from opcodes import Opcodes
from writeback import DestRegMux


def connect_stages(m, stage_from, stage_to):
    for name, sig in stage_from.out.items():
        m.d.comb += stage_to.in_from_prev[name].eq(sig)


class StageWrapper(Elaboratable):
    def __init__(self, prev_stage=None):
        self.in_from_prev = {}
        self.out = {}
        self._in = {}
        self.through = {}
        self.stall_from_top_next = Signal()
        self.stall_from_top = Signal()
        self.add_through_sig('stall', Signal(1))
        self.true_stall = Signal()

        if prev_stage:
            for name, sig in prev_stage.out.items():
                self.add_input_sig(name, sig)

    def add_input_sig(self, name, like):
        setattr(self, "in_from_prev_"+name,
                Signal.like(like, name="in_from_prev_"+name))
        setattr(self, "_in_"+name, Signal.like(like, name="_in_"+name))
        self.in_from_prev[name] = getattr(self, "in_from_prev_"+name)
        self._in[name] = getattr(self, "_in_"+name)

    def add_output_sig(self, name, like):
        setattr(self, "out_"+name, Signal.like(like, name="out_"+name))
        self.out[name] = getattr(self, "out_"+name)

    def add_through_sig(self, name, like):
        self.add_input_sig(name, like)
        self.add_output_sig(name, like)
        self.through[name] = True

    def elaborate(self, platform):
        m = Module()
        true_stall = self.true_stall
        m.d.comb += true_stall.eq(self._in_stall |
                                  self.stall_from_top_next | self.stall_from_top)
        with m.If(~self.stall_from_top):
            for name, sig in self.in_from_prev.items():
                m.d.sync += self._in[name].eq(sig)
        for name, _ in self.through.items():
            m.d.comb += self.out[name].eq(self._in[name])
        m.d.comb += self.out_stall.eq(true_stall)
        return m


class StageDummy(StageWrapper):
    def __init__(self, prev_stage=None):
        super().__init__(prev_stage)
        self.add_input_sig('foo', Signal(32))
        self.add_output_sig('outfoo', Signal(32))

    def elaborate(self, platform):
        m = super().elaborate(platform)
        m.d.comb += self.out_outfoo.eq(self._in_foo + 1)
        return m


class StageIF(StageWrapper):
    def __init__(self, bus, prev_stage=None):
        super().__init__(prev_stage)
        self.add_output_sig('pc', Signal(32))
        self.add_output_sig('instruction', Signal(32))
        self.pc = Signal(32)
        self.bus = bus

    def elaborate(self, platform):
        m = super().elaborate(platform)
        m.d.comb += [
            self.bus.addr.eq(self.pc[2:]),
            self.out_instruction.eq(self.bus.data),
            self.out_pc.eq(self.pc),
        ]
        return m


class StageID(StageWrapper):
    def __init__(self, prev_stage=None):
        super().__init__(prev_stage)
        self.add_through_sig('pc', Signal(32))
        self.add_output_sig('op', Signal(7, decoder=Opcodes))
        self.add_output_sig('imm', Signal(32))
        self.add_output_sig('rs1', Signal(32))
        self.add_output_sig('rs2', Signal(32))

        self.add_output_sig('which_rs2', Signal(5, decoder=RegNames))
        self.add_output_sig('alu_op', Signal(3))
        self.add_output_sig('alu_src1', Signal(2))
        self.add_output_sig('alu_src2', Signal(2))
        self.add_output_sig('jump_en', Signal(1))
        self.add_output_sig('branch_en', Signal(1))
        self.add_output_sig('branch_op', Signal(3))
        self.add_output_sig('shamt_is_rs2', Signal(1))
        self.add_output_sig('sub', Signal(1))
        self.add_output_sig('shift_arithmetic', Signal(1))
        self.add_output_sig('mem_mode', Signal(2, decoder=DataMemMode))
        self.add_output_sig('mem_width', Signal(3))
        self.add_output_sig('which_rd', Signal(5, decoder=RegNames))
        self.add_output_sig('wrd_sel', Signal(2, decoder=ForwardSources))

        self.regfile = RegFile()
        self.imm_decoder = ImmDecoder()
        self.controller = Controller()
        self.forwarder = Forwarder()

        self.rs1_src = Signal(2, decoder=ForwardSources)
        self.rs2_src = Signal(2, decoder=ForwardSources)
        from_ex = Signal(32)
        from_mem = Signal(32)
        from_wb = Signal(32)
        self.mux_rs1 = Array([self.regfile.rs1, from_wb, from_mem, from_ex])
        self.mux_rs2 = Array([self.regfile.rs2, from_wb, from_mem, from_ex])

        self.which_rd_from_wb = Signal(5, decoder=RegNames)
        self.rd_from_wb = Signal(32)
        self.stall_because_read = Signal()
        self.ex_is_mem_read = Signal()
        self.ex_is_jump = Signal()

    def elaborate(self, platform):
        m = super().elaborate(platform)
        regfile = m.submodules.regfile = self.regfile
        imm_decoder = m.submodules.imm_decoder = self.imm_decoder
        controller = m.submodules.controller = self.controller
        forwarder = m.submodules.forwarder = self.forwarder
        which_rs1 = self._in_instruction[15:20]
        which_rs2 = self._in_instruction[20:25]
        which_rd = self._in_instruction[7:12]
        m.d.comb += [
            self.out_op.eq(controller.op),
            controller.instruction.eq(self._in_instruction),
            imm_decoder.instruction.eq(self._in_instruction),
            self.out_imm.eq(imm_decoder.imm),

            regfile.which_rs1.eq(which_rs1),
            forwarder.which_rs1.eq(which_rs1),
            regfile.which_rs2.eq(which_rs2),
            forwarder.which_rs2.eq(which_rs2),
            regfile.which_rd.eq(self.which_rd_from_wb),
            forwarder.which_rd.eq(which_rd),
            forwarder.do_set.eq(~self._in_stall),
            forwarder.build_rd.eq(controller.build_rd & ~(
                self.ex_is_jump) & ~(self.true_stall)),
            regfile.rd.eq(self.rd_from_wb),
            self.rs1_src.eq(forwarder.src[which_rs1]),
            self.rs2_src.eq(forwarder.src[which_rs2]),
            self.out_rs1.eq(self.mux_rs1[self.rs1_src]),
            self.out_rs2.eq(self.mux_rs2[self.rs2_src]),
            self.out_which_rs2.eq(which_rs2),
        ]
        with m.If(controller.write_rd_en):
            m.d.comb += self.out_which_rd.eq(which_rd)

        with m.If(self.ex_is_mem_read):
            with m.If((forwarder.src[which_rs1] == ForwardSources.EX) & (controller.need_rs1 == True)):
                m.d.comb += self.stall_because_read.eq(True)
            with m.If((forwarder.src[which_rs2] == ForwardSources.EX) & (controller.need_rs2 == True)):
                m.d.comb += self.stall_because_read.eq(True)

        for signame in [
            'mem_mode',
            'mem_width',
            'jump_en',
            'branch_en',
            'branch_op',
            'shamt_is_rs2',
            'shift_arithmetic',
            'sub',
            'wrd_sel',
            'alu_src1',
            'alu_src2',
            'alu_op',
        ]:
            m.d.comb += self.out[signame].eq(getattr(controller, signame))

        return m


class StageEX(StageWrapper):
    def __init__(self, prev_stage=None):
        super().__init__(prev_stage)
        self.add_through_sig('op', Signal(7, decoder=Opcodes))

        self.add_through_sig('rs2', Signal(32))

        self.add_output_sig('alu_out', Signal(32))
        self.add_output_sig('incr_pc', Signal(32))

        self.add_through_sig('mem_mode', Signal(2, decoder=DataMemMode))
        self.add_through_sig('mem_width', Signal(3))
        self.add_through_sig('which_rd', Signal(5, decoder=RegNames))
        self.add_through_sig('wrd_sel', Signal(2, decoder=ForwardSources))

        self.alu = Alu()
        self.brancher = Brancher()
        self.do_jump = Signal()
        self.new_pc = self.brancher.new_pc

    def elaborate(self, platform):
        m = super().elaborate(platform)
        alu = m.submodules.alu = self.alu
        brancher = m.submodules.brancher = self.brancher

        m.d.comb += [
            alu.pc.eq(self._in_pc),
            alu.rs1.eq(self._in_rs1),
            alu.rs2.eq(self._in_rs2),
            alu.imm.eq(self._in_imm),
            alu.which_rs2.eq(self._in_which_rs2),
            alu.shamt_is_rs2.eq(self._in_shamt_is_rs2),
            alu.alu_op.eq(self._in_alu_op),
            alu.alu_src1.eq(self._in_alu_src1),
            alu.alu_src2.eq(self._in_alu_src2),
            alu.shift_arithmetic.eq(self._in_shift_arithmetic),
            alu.sub.eq(self._in_sub),
            self.out_alu_out.eq(alu.out),
            brancher.pc.eq(self._in_pc),
            brancher.rs1.eq(self._in_rs1),
            brancher.rs2.eq(self._in_rs2),
            brancher.pc_from_alu.eq(alu.out),
            brancher.branch_en.eq(self._in_branch_en),
            brancher.branch_op.eq(self._in_branch_op),
            brancher.jump_en.eq(self._in_jump_en),
            self.out_incr_pc.eq(brancher.incr_pc),
        ]
        with m.If(~self.true_stall):
            m.d.comb += self.do_jump.eq(self.brancher.do_jump)

        return m


class StageMEM(StageWrapper):
    def __init__(self, r_bus, w_bus, prev_stage=None):
        super().__init__(prev_stage)
        self.add_through_sig('op', Signal(7, decoder=Opcodes))
        self.add_through_sig('alu_out', Signal(32))
        self.add_through_sig('incr_pc', Signal(32))
        self.add_output_sig('mem_read', Signal(32))

        self.add_through_sig('which_rd', Signal(5, decoder=RegNames))
        self.add_through_sig('wrd_sel', Signal(2, decoder=ForwardSources))
        self.datamem = DataMem(r_bus, w_bus)

        self.reg_mux = DestRegMux()
        self.rd = self.reg_mux.rd

    def elaborate(self, platform):
        m = super().elaborate(platform)
        datamem = m.submodules.datamem = self.datamem

        with m.If(self.true_stall == False):
            m.d.comb += [
                datamem.mode.eq(self._in_mem_mode),
                datamem.width.eq(self._in_mem_width),
                datamem.addr.eq(self._in_alu_out),
                datamem.write_data.eq(self._in_rs2),
                self.out_mem_read.eq(datamem.read_data),
            ]

        reg_mux = m.submodules.reg_mux = self.reg_mux
        m.d.comb += [
            reg_mux.alu_out.eq(self._in_alu_out),
            reg_mux.mem_read.eq(datamem.read_data),
            reg_mux.incr_pc.eq(self._in_incr_pc),
            reg_mux.wrd_sel.eq(self._in_wrd_sel),
        ]
        return m


class StageWB(StageWrapper):
    def __init__(self, prev_stage=None):
        super().__init__(prev_stage)

        self.reg_mux = DestRegMux()
        self.rd = self.reg_mux.rd
        self.which_rd = Signal(5, decoder=RegNames)

    def elaborate(self, platform):
        m = super().elaborate(platform)
        reg_mux = m.submodules.reg_mux = self.reg_mux
        m.d.comb += [
            reg_mux.alu_out.eq(self._in_alu_out),
            reg_mux.mem_read.eq(self._in_mem_read),
            reg_mux.incr_pc.eq(self._in_incr_pc),
            reg_mux.wrd_sel.eq(self._in_wrd_sel),
        ]
        with m.If(self.true_stall == False):
            m.d.comb += self.which_rd.eq(self._in_which_rd)
        return m
