#define RVTEST_RV32U
#define TESTNUM gp // why don't the tests set this up? I mean I don't know if they aren't using gp
#define RVTEST_CODE_BEGIN \
        .globl _start; \
        .section .text; \
_start:
#define RVTEST_CODE_END
#define RVTEST_DATA_BEGIN
#define RVTEST_DATA_END
#define RVTEST_FAIL \
    addi a0, TESTNUM, 0; \
    li a1, 0xDEADBEEF; \
    nop; \
    nop; \
    nop; \
    nop; \
    nop; \
    ebreak;
#define RVTEST_PASS \
    addi a0, TESTNUM, 0; \
    li a1, 0x0D15EA5E; \
    nop; \
    nop; \
    nop; \
    nop; \
    nop; \
    ebreak;
