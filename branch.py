from enum import *
from amaranth import *


class BranchOp(Enum):
    # funct3 values for branch operations
    EQ = 0b000
    NOT_EQ = 0b001
    LESS_THAN = 0b100
    GREATER_THAN_OR_EQ = 0b101
    LESS_THAN_UNSIGNED = 0b110
    GREATER_THAN_OR_EQ_UNSIGNED = 0b111


class Brancher(Elaboratable):
    def __init__(self):
        self.pc = Signal(32)
        self.incr_pc = Signal(32)
        self.new_pc = Signal(32)
        self.rs1 = Signal(signed(32))
        self.rs2 = Signal(signed(32))
        self.pc_from_alu = Signal(32)
        self.jump_en = Signal()
        self.branch_en = Signal()
        self.branch_op = Signal(3, decoder=BranchOp)
        self.invalid = Signal()
        self.do_jump = Signal()

    def elaborate(self, platform):
        m = Module()

        do_jump = self.do_jump
        m.d.comb += self.invalid.eq(False)
        m.d.comb += do_jump.eq(0)
        m.d.comb += self.incr_pc.eq(self.pc + 4)

        with m.If(self.jump_en):
            m.d.comb += do_jump.eq(1)
        with m.Elif(self.branch_en):
            with m.Switch(self.branch_op):
                with m.Case(BranchOp.EQ):
                    m.d.comb += do_jump.eq(self.rs1 == self.rs2)
                with m.Case(BranchOp.NOT_EQ):
                    m.d.comb += do_jump.eq(self.rs1 != self.rs2)
                with m.Case(BranchOp.LESS_THAN):
                    m.d.comb += do_jump.eq(self.rs1 < self.rs2)
                with m.Case(BranchOp.GREATER_THAN_OR_EQ):
                    m.d.comb += do_jump.eq(self.rs1 >= self.rs2)
                with m.Case(BranchOp.LESS_THAN_UNSIGNED):
                    m.d.comb += do_jump.eq(self.rs1.as_unsigned()
                                           < self.rs2.as_unsigned())
                with m.Case(BranchOp.GREATER_THAN_OR_EQ_UNSIGNED):
                    m.d.comb += do_jump.eq(self.rs1.as_unsigned()
                                           >= self.rs2.as_unsigned())
                with m.Default():
                    m.d.comb += self.invalid.eq(True)

        with m.If(do_jump):
            m.d.comb += self.new_pc.eq(self.pc_from_alu)
        with m.Else():
            m.d.comb += self.new_pc.eq(self.incr_pc)

        return m
